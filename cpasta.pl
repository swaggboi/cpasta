#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Oct 2020

use strict;
use warnings;
use utf8;
use open qw{:std :utf8}; # Fix "Wide character in print" warning
use Mozilla::CA;
use HTML::TreeBuilder;
#use Data::Dumper;        # Uncomment for debugging

# Set the subreddit using random 0 or 1
my $board = (int rand 2 == 1) ? "copypasta" : "emojipasta";
# Set the URL based on the chosen sub
my $url   = "https://www.reddit.com/r/${board}/";
# Scrape the content into reference
my $root  = HTML::TreeBuilder->new_from_url($url);
# Grab all of the <p/> tags
my @p     = $root->look_down('_tag', 'p');
# Random integer with upper-limit of the number of elements in @p
my $i     = int rand scalar @p;

# Send it
print $p[$i]->as_text(), "\n";
